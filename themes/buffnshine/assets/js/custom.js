(function($) {
  $(document).ready(function(){
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {
    console.log(this.hash);
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "" && $(this.hash).length > 0) {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
      scrollTop: $(hash).offset().top
      }, 800, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
      });
    } // End if
    });

    $("body").on('click', '.open-sidepanel-footer a', function(e) {
      e.preventDefault();
      $('.menubar .main-nav-extra-content .panel-trigger-btn').click();
    });

    $('.wpcf7-checkbox .wpcf7-list-item-label').on('click touchstart', function(){
      $(this).siblings('input[type="checkbox"]').click();
    })
  });
})(jQuery);