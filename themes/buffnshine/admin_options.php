<?php
  /*
  * ReduxFramework Options Config
  */

  if ( ! class_exists( 'Redux' ) ) {
    return;
  }
add_action( 'after_setup_theme', 'include_admin_options', 1 );
function include_admin_options(){
  $opt_name = 'redux_ThemeTek';

  $fields = array(
      array(
          'id' => 'tek-use-two-logos',
          'type' => 'switch',
          'title' => esc_html__('Use two logos in header', 'ekko'),
          'subtitle' => esc_html__('Turn on to use multiple logos in header.', 'ekko'),
          'default' => false,
          'required' => array('tek-logo-style','equals','1'),
      ),
      array(
          'id' => 'tek-logo-secondary',
          'type' => 'media',
          'readonly' => false,
          'url' => true,
          'title' => esc_html__('Primary Image Logo', 'ekko'),
          'subtitle' => esc_html__('Upload primary logo image.', 'ekko'),
          'required' => array('tek-use-two-logos','equals','1'),
      ),
      array(
          'id' => 'tek-logo2-secondary',
          'type' => 'media',
          'readonly' => false,
          'url' => true,
          'title' => esc_html__('Secondary Image Logo', 'ekko'),
          'subtitle' => esc_html__('Upload secondary image logo.', 'ekko'),
          'required' => array('tek-use-two-logos','equals','1'),
      ),
      array(
          'id' => 'tek-logo-secondary-image-size',
          'type' => 'text',
          'class' => 'kd-numeric-input',
          'title' => esc_html__('Logo Size', 'ekko'),
          'subtitle' => esc_html__('Control the logo width (pixels value). Example: 200.', 'ekko'),
          'required' => array('tek-use-two-logos','equals','1'),
      ),
      array(
          'id' => 'tek-mobile-logo-secondary-image-size',
          'type' => 'text',
          'class' => 'kd-numeric-input',
          'title' => esc_html__('Mobile Logo Size', 'ekko'),
          'subtitle' => esc_html__('Control the mobile logo width (pixels value). Example: 140.', 'ekko'),
          'required' => array('tek-use-two-logos','equals','1'),
      ),
  );

  foreach ($fields as $field) {
    Redux::set_field( $opt_name, 'logo', $field );
  }
}

add_action( 'wp_enqueue_scripts', 'buffnshine_enqueue_scripts' );
function buffnshine_enqueue_scripts() {
  add_filter( 'buffnshine-inline-style', 'buffnshine_dynamic_style' );
  $theme_dynamic_css = apply_filters( 'buffnshine-inline-style', '' );
  wp_add_inline_style('child-style', $theme_dynamic_css);
}

function buffnshine_dynamic_style() {
    ob_start(); ?>
    <?php if ( ekko_get_option( 'tek-mobile-logo-secondary-image-size' ) ) : ?>
      @media (max-width: 960px) {
        #logo .logo img.secondary-logo {
          width: <?php echo esc_attr( ekko_get_option( 'tek-mobile-logo-secondary-image-size' ) ) ?>px;
        }
      }
    <?php endif; ?>
    <?php $dynamic_css = ob_get_clean();

    $dynamic_css = ekko_compress_css( $dynamic_css );
    return $dynamic_css;
  }