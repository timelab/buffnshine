<?php
/*
* Template: Team Members Classic
*/

if(!function_exists('buffnshine_kd_team_set_classic')) {
  function buffnshine_kd_team_set_classic($atts,$content = null){
    extract(shortcode_atts(array(
      'title' => '',
      'title_color' => '',
      'position' => '',
      'position_color' => '',
      'description' => '',
      'description_color' => '',
      'image_source' => '',
      'image' => '',
      'ext_image' => '',
      'ext_image_size' => '',
      'tm_phone' => '',
      'tm_email' => '',
      'tm_disable_contact_effect' => '',
      'team_bg_color' => '',
      'facebook_url' => '',
      'instagram_url' => '',
      'twitter_url' => '',
      'linkedin_url' => '',
      'github_url' => '',
      'xing_url' => '',
      'vk_url' => '',
      'social_color' => '',
      'team_external_url' => '',
      'team_link_text' => '',
      'team_link_target' => '',
      'css_animation' => '',
      'elem_animation_delay' => '',
      'team_extra_class' => '',
    ),$atts));

    $animation_delay = $default_src = $dimensions = $hwstring = $a_attrs = $socials_disabled = $wrapper_class = $disable_contact_effect = '';

    $image = wpb_getImageBySize($params = array(
        'post_id' => NULL,
        'attach_id' => $image,
        'thumb_size' => 'full',
        'class' => ""
    ));

    $default_src = vc_asset_url( 'vc/no_image.png' );
    $dimensions = vc_extract_dimensions( $ext_image_size );
    $hwstring = $dimensions ? image_hwstring( $dimensions[0], $dimensions[1] ) : '';

    // Display helper class if socials are disabled
    if ( empty( $facebook_url ) && empty( $instagram_url ) && empty( $twitter_url ) && empty( $linkedin_url ) && empty( $github_url ) && empty( $xing_url ) && empty( $vk_url ) ) {
      $socials_disabled = 'socials-disabled';
    }

    //CSS Animation
    if ($css_animation == "no_animation") {
        $css_animation = "";
    }

    // Animation delay
    if ($elem_animation_delay) {
        $animation_delay = 'data-animation-delay='.$elem_animation_delay;
    }

    $wrapper_class = implode(' ', array('team-member', 'design-classic', $css_animation, $socials_disabled, $tm_disable_contact_effect, $team_extra_class));

    $output = '<div class="'.trim($wrapper_class).'" '.$animation_delay.'>
                    <div class="team-content">
                        <div class="team-content-text" '.(!empty($team_bg_color) ? 'style="background-color: '.$team_bg_color.';"' : '').'>
                        <div class="team-content-text-inner">
                        <h5 '.(!empty($title_color) ? 'style="color: '.$title_color.';"' : '').'>'.$title.'</h5>
                        <span class="team-subtitle" '.(!empty($position_color) ? 'style="color: '.$position_color.';"' : '').'>'.$position.'</span>
                        <p '.(!empty($description_color) ? 'style="color: '.$description_color.';"' : '').'>'.$description.'</p>';
              if($tm_phone || $tm_email) {
              $output .= '<p '.(!empty($description_color) ? 'style="color: '.$description_color.';"' : '').'>';
              $output .= ($tm_phone) ? 'Tel: <a href="tel:'.$tm_phone.'">'.$tm_phone.'</a>':'';
              $output .= ($tm_phone && $tm_email) ? '<br />' : '';
              $output .= ($tm_email)? '<a href="mailto:'.$tm_email.'">[E-post]</a>' : '';
              $output .= '</p>';
            }
                        if ($team_external_url && $team_link_text) {
                          $output .= '<p class="team-link"><a href="'.$team_external_url.'" target="'.$team_link_target.'">'.$team_link_text.'</a></p>';
                        }

                        $output .= '</div>';
                    $output .='</div>
                    </div>
                </div>';
    return $output;
  }
}
