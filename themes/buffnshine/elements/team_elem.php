<?php

add_action( 'after_setup_theme', 'remove_tek_team_shortcodes',100 );
function remove_tek_team_shortcodes() {
  remove_shortcode( 'tek_team' );
  add_shortcode ( 'tek_team', 'buffnshine_tek_team', 1 );
}


// Render the element on front-end
function buffnshine_tek_team($atts, $content = null) {
  $design_style = '';

  extract(shortcode_atts(array(
      'design_style' => '',
  ), $atts));

  $output = '';
  if ($design_style == 'classic' || $design_style == 'creative') {
    if ($design_style == 'classic') {
      require_once(get_stylesheet_directory().'/elements/templates/team-elem/team-'.$design_style.'.php');
    $template_func = 'buffnshine_kd_team_set_'.$design_style;
    }else {
      require_once(KEYDESIGN_PLUGIN_PATH.'/elements/templates/team-elem/team-'.$design_style.'.php');
    $template_func = 'kd_team_set_'.$design_style;
    }
    
  $output .= $template_func($atts,$content);
  }

  return $output;
}
?>
