<?php
/**
 * The template for displaying the footer
 * Contains the closing of the #wrapper div and all content after.
 *
 * @package ekko
 * by KeyDesign
 */

 $footer_wrapper_class = $footer_fixed_class = $link_hover_effect = $footer_active_widgets = '';

 $footer_active_widgets = is_active_sidebar( 'footer-first-widget-area' ) + is_active_sidebar( 'footer-second-widget-area' ) + is_active_sidebar( 'footer-third-widget-area' ) + is_active_sidebar( 'footer-fourth-widget-area' );

 if ( ekko_get_option( 'tek-footer-fixed' ) == '1') {
   $footer_fixed_class ='fixed';
 } else {
   $footer_fixed_class ='classic';
 }

 if ( '' != ekko_get_option( 'tek-footer-link-hover-effect' ) ) {
   $link_hover_effect = ekko_get_option( 'tek-footer-link-hover-effect' );
 } else {
   $link_hover_effect = 'default-footer-link-effect';
 }

 $footer_wrapper_class = implode(' ', array($footer_fixed_class, $link_hover_effect));
?>

</div>
<div class="mailchimp" style="background-color: #4a4a4a">
  <!-- Begin Mailchimp Signup Form -->
<style type="text/css">
  #mc_embed_signup{background:transparent; clear:both; margin-left:  auto; margin-right: auto; max-width: 600px;}
  #mc_embed_signup form { margin: 0; }
  #mc_embed_signup form .pcf7-text { 
    box-shadow: 0 25px 98px 0 rgb(0 0 0 / 10%);
    background-color: #fff !important;
        -webkit-transition: all 250ms ease-in-out;
    -moz-transition: all 250ms ease-in-out;
    -o-transition: all 250ms ease-in-out;
    transition: all 250ms ease-in-out;
    width: 60%;
    box-sizing: border-box;
    border: none;
    margin-bottom: 10px;
    background-color: #fff;
    box-shadow: none;
    font-size: 16px;
    letter-spacing: 0;
    outline: none;
    line-height: 50px;
    height: 52px;
    padding: 0 30px;
    text-align: left;
    font-family: inherit;
    border-radius: 0px;
    color:  #000;
    float: left;
  }

  .formText { padding: 10px 0; }

  
  #mc_embed_signup h2 {color: #fff; margin-bottom: 5px; }
  #mc_embed_signup .tt_button { margin-left: 0; float: left; width: 40%; }
  div.mailchimp { padding: 40px 20px 50px; color: #fff; }
  /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
     We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
    <form action="https://diteclulea.us21.list-manage.com/subscribe/post?u=5aff3647ef5a9630438d87037&amp;id=be1b074555&amp;f_id=000bf7e1f0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
      <div id="mc_embed_signup_scroll">
        <h2>Missa inga erbjudanden!</h2>
        <div class="formText">Anmäl dig till vårat nyhetsbrev för att ta del av tips och trix.</div>
        <div class="mc-field-group">
          <div>
            <input type="email" value="" name="EMAIL" class="required pcf7-text" id="mce-EMAIL" placeholder="E-post" required>
            <input type="submit" value="Prenumerera" name="subscribe" id="mc-embedded-subscribe" class="tt_button tt_primary_button btn_primary_color button-action-link">
            <div style="clear: both;"></div>
          </div>
        </div>
      </div>
      <div id="mce-responses" class="clear foot">
        <div class="response" id="mce-error-response" style="display:none"></div>
        <div class="response" id="mce-success-response" style="display:none"></div>
      </div>
      <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
      <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_5aff3647ef5a9630438d87037_be1b074555" tabindex="-1" value=""></div>
    </form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
</div>
<footer id="footer" class="<?php echo esc_attr( $footer_wrapper_class ); ?>">
  <div class="upper-footer">
    <div class="container">
      <?php if( ekko_get_option( 'tek-footer-bar' ) == 1 ) : ?>
        <div class="footer-bar <?php if ( ekko_get_option( 'tek-upper-footer' ) == "0") { echo " no-upper-footer"; } ?>">
          <?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
            <div class="footer-nav-menu">
              <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'depth' => 1, 'container' => false, 'menu_class' => 'navbar-footer', 'fallback_cb' => 'false' ) ); ?>
            </div>
          <?php endif; ?>
          <?php if( class_exists( 'ReduxFramework' ) ) : ?>
            <div class="footer-socials-bar">
                <?php echo do_shortcode('[social_profiles]'); ?>
            </div>
          <?php endif; ?>
        </div>
      <?php endif; ?>

      <?php if ( ekko_get_option( 'tek-upper-footer' ) == "1" ) : ?>
        <?php if( $footer_active_widgets >= "1" ) : ?>
          <div class="footer-widget-area">
            <?php get_sidebar( 'footer' ); ?>
          </div>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  </div>
  <div class="lower-footer">
    <div class="container">
       <div class="flex-container">
         <span>
           <?php if ( ekko_get_option( 'tek-footer-text' ) ) {
             echo wp_kses( ekko_get_option( 'tek-footer-text' ), ekko_allowed_html_tags() );
           } else {
             echo esc_html('Ekko by KeyDesign. All rights reserved.');
           } ?>
         </span>
         <?php if( class_exists( 'ReduxFramework' ) ) : ?>
           <div class="footer-socials-bar">
               <?php echo do_shortcode('[social_profiles]'); ?>
           </div>
         <?php endif; ?>
       </div>
   </div>
  </div>
</footer>
<?php /* Back to top button template */ ?>
<?php if ( ekko_get_option( 'tek-backtotop' ) == "1") : ?>
    <div class="back-to-top">
       <i class="fa fa-angle-up"></i>
    </div>
<?php endif; ?>
<?php /* END Back to top button template */ ?>

<?php if ( ekko_get_option( 'tek-header-button-action' ) == '1' ) : ?>
  <?php get_template_part( 'core/templates/header/content', 'modal-box' ); ?>
<?php endif; ?>

<?php if ( ekko_get_option( 'tek-panel-button-action' ) == '1' ) : ?>
  <?php get_template_part( 'core/templates/header/content', 'side-panel' ); ?>
<?php endif; ?>

<?php wp_footer(); ?>
</body>
</html>
